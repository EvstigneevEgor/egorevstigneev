//module.exports.array_counter = function(Array) // второй способ
var array_counter = function(Array)
{
    return"в массиве "+ Array.length + " элементов"
}
//module.exports.arbitrary_function = function(x,z) // второй способ
var arbitrary_function = function(x,z)
{
    //return `это переменная №1 $(x) а это №2 $(z) ` не работает
    return "это переменная №1 "+ (x) +" а это №2 "+ (z) 
}
//module.exports.some_value = 282 ; // второй способ
var some_value = 282;
module.exports.array_counter = array_counter;//на 3 и на 3 способе это не нужно
module.exports.arbitrary_function = arbitrary_function;//на 3 и на 3 способе это не нужно
module.exports.some_value = some_value;//на 3 и на 3 способе это не нужно
/*
module.exports = {
    array_counter:array_counter;
    arbitrary_function:arbitrary_function;
    some_value=some_value;

}
*/