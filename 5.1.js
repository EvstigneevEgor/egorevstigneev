var events = require('events');
var util = require('util');
var cars = function(model){
    this.model = model
};
util.inherits(cars, events.EventEmitter);
var bmw = new cars('BMW');
var audi = new cars('Audi');
var volvo = new cars('Volvo');
var cars = [bmw, audi, volvo];
cars.forEach(function(car) {
    car.on('speed',function(text){
console.log(car.model + " speed is "+ text)
    });
});
bmw.emit('speed','254 km');
audi.emit('speed','25 km');
volvo.emit('speed','4 km');